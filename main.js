// asynchronous
// console.log(1);
// $.ajax({
//   url: 'https://itp-api.herokuapp.com/songs',
//   type: 'get', // optional if get request
//   success: function(response) {
//     console.log(response);
//     console.log(3);
//   },
//   error: function(jqXHR, textStatus, errorThrown) {
//     alert('Ooops something went wrong');
//   }
// });
// console.log(2);

// var promise = $.ajax({
//   url: 'https://itp-api.herokuapp.com/songs'
// });
//
// promise
//   .then(function(response) {
//     console.log('r1', response);
//     return response.songs;
//   }, function() {
//     console.log('error');
//   })
//   .then(function(response) {
//     console.log('r2', response);
//   });

// resolved, rejected, or pending
//
// function getSongs() {
//   var promise = $.ajax({
//     url: 'https://itp-api.herokuapp.com/songs'
//   });
//
//   return promise.then(function(response) {
//     console.log('r1', response);
//     return response.songs;
//   }, function() {
//     console.log('error');
//   });
// }
//
// getSongs().then(function(songs) {
//   console.log(songs);
// });

// promise
//   .then(function(response) {
//     console.log('r1', response);
//     return response.songs;
//   }, function() {
//     console.log('error');
//   })
//   .then(function(response) {
//     console.log('r2', response);
//   });

// var songs, artists;
//
// function bothHaveComplete() {
//   if (songs && artists) {
//     console.log(songs, artists);
//   }
// }
//
// $.ajax({
//   url: 'https://itp-api.herokuapp.com/songs',
//   success: function(response) {
//     console.log(response.songs);
//     songs = response.songs;
//     bothHaveComplete();
//   }
// });
//
// $.ajax({
//   url: 'https://itp-api.herokuapp.com/artists',
//   success: function(response) {
//     console.log(response.artists);
//     artists = response.artists;
//     bothHaveComplete();
//   }
// });

// callback hell
// $.ajax({
//   url: 'https://itp-api.herokuapp.com/songs',
//   success: function(response) {
//     var songs = response.songs;
//
//     $.ajax({
//       url: 'https://itp-api.herokuapp.com/artists',
//       success: function(response) {
//         var artists = response.artists;
//
//         console.log(songs, artists);
//       }
//     });
//   }
// });

// var songs = $.ajax({
//   url: 'https://itp-api.herokuapp.com/songs'
// });
//
// var artists = $.ajax({
//   url: 'https://itp-api.herokuapp.com/artists'
// });
//
// $.when(songs, artists).then(function(songsResponse, artistsResponse) {
//   console.log(songsResponse, artistsResponse);
// });


function getSongs() {
  return $.ajax({
    url: 'https://itp-api.herokuapp.com/songs'
  }).then(function(response) {
    return response.songs;
  });
}

function getArtists() {
  return $.ajax({
    url: 'https://itp-api.herokuapp.com/artists'
  }).then(function(response) {
    return response.artists;
  });
}

$.when(getSongs(), getArtists()).then(function(songs, artists) {
  console.log('songs', songs);
  console.log('artists', artists);
});
